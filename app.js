

require('./minInventorySettingConfig');



productTypeInfoList = [];
productEPClist = [];
minInventorySettings = [];
WebSocketEventCenter = null;



/**
 * Module dependencies.
 */
 var colors = require('colors');

 colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red'
});
 var EventProxy = require('eventproxy');
 g_EventProxy = new EventProxy();

 var Q        = require('q');

 var request = require('request');
 httpRequestGet = Q.denodeify(request.get);
 httpRequestPost = Q.denodeify(request.post);


 var express = require('express');
 var routes = require('./routes');
 var http = require('http');
 var path = require('path');
 var wsServerInventory = require('./routes/wsServerInventory');

 var app = express();

 var server = wsServerInventory.startWebSocketServer(app);
// all environments
app.set('port', process.env.PORT || httpListeningPort);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/carbinet', routes.carbinet);
app.get('/1', routes.carbinet);
app.get('/2', routes.inventoryAlert);
app.get('/inventoryAlert', routes.inventoryAlert);
app.get('/', routes.inventoryAlert);


server.listen(app.get('port'), function(){
  console.log(('Inventory Carbinet server listening on port ' + app.get('port')).info);
});
