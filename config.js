
IntervalRequestEpcInfo = 60 * 1000;
IntervalCheckInventory = 30 * 1000;
IntervalInventoryAlert = 10 * 60 * 1000;
EpcServerIP = "127.0.0.1";
// EpcServerIP = "192.168.48.121";
wsUriEventCenter = "ws://127.0.0.1:6013";

defaultMinTagReadedConfirmCount = 2;
defaultmaxTagReadInterval = 1000 * 10;

readerPortNameMapList = [
{
  port:5100
  , readerName:'reader1'
  , minTagReadedConfirmCount: 2
  , maxTagReadInterval: 1000 * 15}, 
  
  {
    port:5210
    , minTagReadedConfirmCount: 2
    , maxTagReadInterval: 1000 * 10
    , readerName:'reader2'}

];//


minInventorySettingConfig = [{productCode: '01', value: 2}, {productCode: '02', value: 4}, {productCode: '10', value: 4}];

httpListeningPort = 6001;//http端口
