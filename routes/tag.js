
module.exports = Tag;

var _ = require("underscore");
var timeFormater = require('./timeFormat').getCurrentTime;

function Tag(_epc, _ep){
	var self = this;
	self.epc = _epc;
	self.records = [];
	self.ep = _ep;
}
Tag.prototype.addRecord = function(_readedCount, _antenaID){
	var self = this;
	// if(self.epc != 'AD94240019F3CD8E4E000017') return;
	var timeStamp = (new Date()).getTime();
	var record = {readCount: _readedCount, antenaID: _antenaID, timeStamp: timeStamp};
	if(_.size(self.records) > 5){
		// console.log('before:');
		// console.dir(self.records);
		self.records = _.initial(_.sortBy(self.records, function(_record){
			return -_record.timeStamp;
		}));
		// console.log('after:');
		// console.dir(self.records);
	}
	record.epc = self.epc;
	if(_.size(self.records) > 0){
		// console.dir(self.records);
		// var latestRecord = self.records[0];
		var latestRecord = _.max(self.records, function(_record){
			return _record.timeStamp;
		});
		// console.log('record: ' + record.timeStamp)
		// console.log('latest: ' + latestRecord.timeStamp);
		record.interval = timeStamp - latestRecord.timeStamp;
	}else{
		record.interval = 0;
	}
	// console.log('interval => ' + record.interval);
	self.records.unshift(record);
	self.ep.emit('newRecord', record);
}

