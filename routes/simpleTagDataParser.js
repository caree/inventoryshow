
module.exports = simpleTagDataParser;

var EventProxy = require('eventproxy');

var dgram = require("dgram");
var _ = require("underscore");
var timeFormater = require('./timeFormat').getCurrentTime;
var Tag = require('./tag');

function simpleTagDataParser(_options){
	var self = this;
	self.options = _options;
	self.server = null;//UDP Server
	self.tagList=[];
	self.listeningPort = self.options.listeningPort || 65500;
	self.eventID = '' + self.listeningPort;
	self.globalEp = self.options.ep || null;
	self.privateEp = new EventProxy();
	self.privateEp.tail('newRecord', function(_record){
		console.log((self.listeningPort + ' newRecord: ' + _record.epc).data);
		if(self.globalEp !== null){
			// console.log('globalEp => ' + self.eventID);
			self.globalEp.emit('newRecord' + self.eventID, _record);
		}
	});
}
simpleTagDataParser.prototype.getTagList = function(){
	return this.tagList;
};
simpleTagDataParser.prototype.startServer = function(){
	var self = this;
	if(self.server !== null) return;

	self.server = dgram.createSocket("udp4");

	self.server.on("error", function (err) {
		console.log("self.server error:\n" + err.stack);
		self.server.close();
		self.server = null;
	});

	self.server.on("message", function (msg, rinfo) {
		console.log((timeFormater() + " : " + msg + " from " + rinfo.address + ":" + rinfo.port).data);
		// console.log(timeFormater() + " : " + msg + " from " + rinfo.address + ":" + rinfo.port);
		
		self.parseMultiItem(String(msg));
	});

	self.server.on("listening", function () {
		var address = this.address();
		console.log("server listening " +
			address.address + ":" + address.port);
		});

	self.server.bind(self.listeningPort);
};

simpleTagDataParser.prototype.parseMultiItem = function(_multiItem){
	var self = this;
	// console.log(_multiItem.error);
	var items = _multiItem.match(/\[[\w]{1,},[0-9a-f]+\]/g);
	// console.dir(items);
	_.each(items, function(_item){
		self.parseSingleItem(_item);
	});
};

// like "Disc:2000/02/28 20:01:51, Last:2000/02/28 20:07:42, Count:00019, Ant:02, Type:04, Tag:300833B2DDD906C001010101 "
simpleTagDataParser.prototype.parseSingleItem = function(_item){
	var self = this;
	// console.log(_item.error);
	var dotIndex = _item.indexOf(',');
	// var tagFlagIndex = _item.indexOf('Tag:');
	// console.log(tagFlagIndex);
	// var countFlagIndex = _item.indexOf('Count:');
	// var antFlagIndex = _item.indexOf('Ant:');
	var flag = _item.substr(1, dotIndex-1);
	var epc = _item.substr(dotIndex + 1, (_item.length - dotIndex - 2));
	var antennaID = flag;
	var count = 1;
	console.log(epc.error);
	var findedTag = _.find(self.tagList, function(_tag){
		return _tag.epc == epc;
	});
	if(findedTag == null){
		var newTag = new Tag(epc, self.privateEp);
		self.tagList.push(newTag);
		newTag.addRecord(count, antennaID);
	}else{
		// console.dir(findedTag);
		findedTag.addRecord(count, antennaID);
	}
};

