
module.exports = InventoryLogicUnit;


var _ = require("underscore");
var timeFormater = require('./timeFormat').getCurrentTime;
var dataParser = require('./dataParser');
var simpleParser = require('./simpleTagDataParser');
var EventProxy = require('eventproxy');

//生成一个数组自身的阶差，例如 [15,9,7,6,1] 生成  [ 6, 2, 1, 5 ]
Array.prototype.mapMinusSelf = function(){
    if(this.length <= 1) return [];
    var mapMinus = [];
    for(var i = 0, len = this.length; i < len -1; i++){
        mapMinus.push(this[i] - this[i+1]);
    }

    return mapMinus;
}
//给一个时间参数和时间代理，将标签存在变化通过事件方式传送出去
function InventoryLogicUnit(_options){
	var self = this;
	self.options = _options || {};
	self.minSensitiveCount = self.options.minSensitiveCount || 1;
	self.maxTagReadInterval = self.options.maxTagReadInterval || 10000; 
	self.minTagReadedConfirmCount = self.options.minTagReadedConfirmCount || 1;
	self.listeningPort = self.options.listeningPort;
	self.globalEp = self.options.ep || null;
	self.parserType = self.options.parserType || 'Reader';//大读写器

	// self.privateEp = new EventProxy();

	// self.privateEp.tail('newRecord' + self.listeningPort, function(_record){
	// 	console.log(self.listeningPort + ' newRecord: ' + _record.epc);
	// 	// self.checkTagAppear(_record);
	// });
	// self.privateEp.tail('newRecord', function(_record){
	// 	if(self.globalEp != null){
	// 		// console.log('globalEp => ' + self.eventID);
	// 		self.globalEp.emit('newRecord' + self.eventID, _record);
	// 	}
	// });
	if(self.parserType == 'Reader'){
		self.parser = new dataParser({listeningPort: self.listeningPort});
	}else if(self.parserType == 'simpleReader'){
		self.parser = new simpleParser({listeningPort: self.listeningPort});
	}
	self.tagList = self.parser.getTagList();
	self.lastExistsTagList = [];


	self.timerID = null;
}
InventoryLogicUnit.prototype.emitTagEvent = function(_eventID, _data){
	var self = this;
	if(self.globalEp != null){
		self.globalEp.emit(_eventID+self.listeningPort, _data);
	}
}
InventoryLogicUnit.prototype.startServer = function() {
	// body...
	var self = this;
	self.parser.startServer();

	var checkTagChanges = function(_inventoryUnit){
		_inventoryUnit.checkTagChanges();
	}

	self.timerID = setInterval(checkTagChanges, 500, self);
};

InventoryLogicUnit.prototype.getCurrentExistsTagList = function(){
	var self = this;
	// console.log('getCurrentExistsTagList ...');
	var currentTimeStamp = (new Date()).getTime();
	var currentExistsTagList = _.without(_.map(self.tagList, function(_tag){
		var records = _tag.records;
		// 至少有多次的读取记录才能参与是否真正读取的选择
		if(_.size(records) <= self.minTagReadedConfirmCount){
			console.info((_tag.epc + ': no enough records').warn);
			return null;
		} 
		records = _.first(records, self.minTagReadedConfirmCount);
		if(_.every(records, function(_record){

			if(_record.readCount < self.minSensitiveCount) {
				console.log((_record.epc + ' too less read!!!').warn);
				return false;
			}
			return true;
		}) == false){
			return null;
		}

		var timeStamps = _.pluck(records, 'timeStamp');
    	timeStamps.unshift(currentTimeStamp);//插入当前时间，为生成阶差做准备
    	var mapMinus = timeStamps.mapMinusSelf();
    	if(_.contains(_.map(mapMinus, function(_minusResult){
    		return _minusResult >= (self.maxTagReadInterval);
    	}), true)){
    		console.log((_tag.epc + ':  there is a too large interval !!!!').warn);
    		console.dir(mapMinus);
    		return null;
    	}

		return {epc: _tag.epc, antenaID: _tag.records[0].antenaID};
	}), null);
	// console.dir(currentExistsTagList);
	return currentExistsTagList;
}
InventoryLogicUnit.prototype.checkTagChanges = function(){
	var self = this;
	// console.log('checkTagChanges ...'.error);
	// console.dir(self);
	var currentExistsTagList = self.getCurrentExistsTagList();
	var currentExistsEpcs = _.pluck(currentExistsTagList, "epc");
	var lastExistsEpcs = _.pluck(self.lastExistsTagList, "epc");

	var epcsDisappear = _.difference(lastExistsEpcs, currentExistsEpcs);
	if(_.size(epcsDisappear) > 0){
		var tagsDisappear = _.without(_.map(self.lastExistsTagList, function(_tag){
			if(_.contains(epcsDisappear, _tag.epc)){
				_tag.Event = "deleted";
				console.log(('deleted tag ' + _tag.epc).info);
				return _tag;
			}else{
				return null;
			}
		}), null);
		self.emitTagEvent('tagDeleted', tagsDisappear);
	}

	var epcsAppear = _.difference(currentExistsEpcs, lastExistsEpcs);
	if(_.size(epcsAppear) > 0){
		var tagsAppear = _.without(_.map(currentExistsTagList, function(_tag){
			if(_.contains(epcsAppear, _tag.epc)){
				_tag.Event = "added";			
				console.log(('new tag ' + _tag.epc).info);
				return _tag;
			}else{
				return null;
			}
		}) ,null);
		self.emitTagEvent('tagAdded', tagsAppear);
	}
	self.lastExistsTagList = currentExistsTagList;	
};



//***********************************************




