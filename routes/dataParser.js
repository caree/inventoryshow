
module.exports = dataParser;

var EventProxy = require('eventproxy');

var dgram = require("dgram");
var _ = require("underscore");
var timeFormater = require('./timeFormat').getCurrentTime;
var Tag = require('./tag');

function dataParser(_options){
	var self = this;
	self.options = _options;
	self.server = null;//UDP Server
	self.tagList=[];
	self.listeningPort = self.options.listeningPort || 65500;
	self.eventID = '' + self.listeningPort;
	self.globalEp = self.options.ep || null;
	self.privateEp = new EventProxy();
	self.privateEp.tail('newRecord', function(_record){
		console.log((self.listeningPort + ' newRecord: ' + _record.epc).data);
		if(self.globalEp !== null){
			// console.log('globalEp => ' + self.eventID);
			self.globalEp.emit('newRecord' + self.eventID, _record);
		}
	});
}
dataParser.prototype.getTagList = function(){
	return this.tagList;
};
dataParser.prototype.startServer = function(){
	var self = this;
	if(self.server !== null) return;

	self.server = dgram.createSocket("udp4");

	self.server.on("error", function (err) {
		console.log("self.server error:\n" + err.stack);
		self.server.close();
		self.server = null;
	});

	self.server.on("message", function (msg, rinfo) {
		console.log((timeFormater() + " : " + msg + " from " + rinfo.address + ":" + rinfo.port).data);
		// console.log(timeFormater() + " : " + msg + " from " + rinfo.address + ":" + rinfo.port);
		
		self.parseMultiItem(String(msg));
	});

	self.server.on("listening", function () {
		var address = this.address();
		console.log("server listening " +
			address.address + ":" + address.port);
		});

	self.server.bind(self.listeningPort);
};

dataParser.prototype.parseMultiItem = function(_multiItem){
	var self = this;
	var items = _multiItem.match(/Disc:\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}[,]\sLast:\d{4}\/\d{2}\/\d{2}\s\d{2}:\d{2}:\d{2}[,]\sCount:\d{5}[,]\sAnt:\d{2}[,]\sType:\d{2}[,]\sTag:[0-9A-F]{24}/g);
	// console.dir(items);
	_.each(items, function(_item){
		self.parseSingleItem(_item);
	});
};

// like "Disc:2000/02/28 20:01:51, Last:2000/02/28 20:07:42, Count:00019, Ant:02, Type:04, Tag:300833B2DDD906C001010101 "
dataParser.prototype.parseSingleItem = function(_item){
	var self = this;
	var tagFlagIndex = _item.indexOf('Tag:');
	// console.log(tagFlagIndex);
	var countFlagIndex = _item.indexOf('Count:');
	var antFlagIndex = _item.indexOf('Ant:');

	var epc = _item.substr(tagFlagIndex + 4, 24);
	var antennaID = _item.substr(antFlagIndex+4, 2);
	var count = parseInt(_item.substr(countFlagIndex+6, 5));
	// var count = new Number(_item.substr(countFlagIndex+6, 5)).toFixed();

	var findedTag = _.find(self.tagList, function(_tag){
		return _tag.epc == epc;
	});
	if(findedTag == null){
		var newTag = new Tag(epc, self.privateEp);
		self.tagList.push(newTag);
		newTag.addRecord(count, antennaID);
	}else{
		// console.dir(findedTag);
		findedTag.addRecord(count, antennaID);
	}
};

