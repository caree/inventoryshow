
/*
目标：
1. 每隔一定时间产生模拟数据
2. 产生模拟数据的间隔可以设置

*******************************************/ 

var dgram = require('dgram');
var _ = require("underscore");



var rawTagInfoList = 
[
		// "Disc:2000/02/28 20:01:51, Last:2000/02/28 20:07:42, Count:00019, Ant:02, Type:04, Tag:300833B2DDD906C001010101   "
		// , "Disc:2000/02/28 20:01:51, Last:2000/02/28 20:07:42, Count:00019, Ant:02, Type:04, Tag:300833B2DDD906C001010102   "
		// , "Disc:2000/02/28 20:01:51, Last:2000/02/28 20:07:42, Count:00019, Ant:02, Type:04, Tag:300833B2DDD906C001010103   "
		'[7301,A001]'
		,'[7302,A002]'
];
var client;
var destIP = 'localhost';
var destPort = '5301';

start();

function start(){
	initial();
	setInterval(sendRawTagInfo, 2000);

}

function initial(){
	client = dgram.createSocket("udp4");
	client.on("error", function (err) {
	  console.log("client error:\n" + err.stack);
	  client.close();
	});
}

function sendRawTagInfo(){
	_.each(rawTagInfoList, function(_rawTagInfo){
		var message = new Buffer(_rawTagInfo);
		client.send(message, 0, message.length, destPort, destIP, function(err, bytes) {
		  // client.close();

		});

	});

}





